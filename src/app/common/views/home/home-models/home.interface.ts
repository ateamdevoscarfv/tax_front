export interface HomeModel {
    Codigo: number;
    Cuenta: number;
    FechaCreacion: string;
    FechaHora: string;
    Observaciones: string;
    Recibo: null;
    Usuario: number;
    in: In;
    out: Out;
}

export interface In {
    detalleoperacion_clientecodigo: string;
    detalleoperacion_clientedescripcion: string;
    detalleoperacion_codigo: number;
    detalleoperacion_codigotipocarga: number;
    detalleoperacion_codigovagoneta: number;
    detalleoperacion_codigovehiculo: string;
    detalleoperacion_conductorcodigo: number;
    detalleoperacion_conductordescripcion: string;
    detalleoperacion_cuenta: number;
    detalleoperacion_descripciontipocarga: string;
    detalleoperacion_fechahora: string;
    detalleoperacion_imagenes: string;
    detalleoperacion_lugar: string;
    detalleoperacion_observacion: string;
    detalleoperacion_operacion: number;
    detalleoperacion_peso: string;
    detalleoperacion_placavehiculo: string;
    detalleoperacion_tipooperacion: string;
    detalleoperacion_usuario: number;
}

export interface Out {
    detalleoperacion_clientecodigo: string;
    detalleoperacion_clientedescripcion: string;
    detalleoperacion_codigo: number;
    detalleoperacion_codigotipocarga: number;
    detalleoperacion_codigovagoneta: number;
    detalleoperacion_codigovehiculo: string;
    detalleoperacion_conductorcodigo: number;
    detalleoperacion_conductordescripcion: string;
    detalleoperacion_cuenta: number;
    detalleoperacion_descripciontipocarga: string;
    detalleoperacion_fechahora: string;
    detalleoperacion_imagenes: string;
    detalleoperacion_lugar: string;
    detalleoperacion_observacion: string;
    detalleoperacion_operacion: number;
    detalleoperacion_peso: string;
    detalleoperacion_placavehiculo: string;
    detalleoperacion_tipooperacion: string;
    detalleoperacion_usuario: number;
}

export interface Users {
    id: number;
    name: string;
    username: string;
    email: string;
    address: Address;
    geo: Geo;
    phone: string;
    website: string;
    company: Company;
}


export interface Address {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
}


export interface Company {
    name: string;
    catchPhrase: string;
    bs: string;
}

export interface Geo {
    lat: string;
    lng: string;
}
