import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 
import { tablereload } from '../home-table/home-table.component';
import { HomeIvaSevice } from '../home-iva/home-iva.service.';

var imprime;
@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent  implements OnInit  {

   date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  firstFormGroup: FormGroup;
    public url = environment.api+"/setEndOfMonth";
  public imprime2; 
  resp;
  public userbasic;
  infos = {
    info : {
      name:"",
      start_date:"",
      end_date:""

    }
    
  };

constructor(public homeIva: HomeIvaSevice, private table: tablereload, public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { 
this.userbasic = JSON.parse(localStorage.getItem("usuario"));
}
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        nombre: ['', Validators.required],
       
        
      });
       

    }
    public carga(a){
        this.imprime2 = a;
        console.log(this.imprime2);
        
    }
 
    guardar(){

      this.infos = {
        info : {
          name: this.firstFormGroup.value.nombre,
          start_date: this.vefecha(this.date.value),
          end_date: this.vefecha(this.serializedDate.value)
        }
      }
      this.http.postdevice(this.url,JSON.stringify(this.infos),this.userbasic.info).subscribe(data => {
          this.resp = data;
          if(this.resp.status != "Error"){
            this.toastr.success(this.resp.message)
            this.reloadComponent();
          }else{
             this.toastr.error(this.resp.message)
          }
      })
      
    }
       vefecha(a){
        var esp = " ";
        var b = JSON.stringify(a); 
        var c = b.split("-",3);
        var k = c[0].slice(1);
        var z = c[1]
        var m = c[2].slice(0,-15)
        return  k+"-"+z+"-"+m;
        
        
        }
      
  
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
}