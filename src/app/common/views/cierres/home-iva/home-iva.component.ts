import { Component, OnInit,Input, ViewChild, AfterViewInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HomeTableComponent } from '../home-table/home-table.component';
@Component({
  selector: 'app-iva',
  templateUrl: './home-iva.component.html',
  styleUrls: ['./home-iva.component.scss']
})
export class HomeIvaComponent implements OnInit {
    public selected = 'ventas';     
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    @Input() tabladatos;
    @Input() fecha;
    @Input() userbasic;
    @Input()
    @Input() urlgetcompra = environment.api+"/getPurchases/";
    @Input() urlgetventa = environment.api+"/getSales/";
    public ventas = false;
    envio = {
        datos: '',
        fecha:'',
        usuario: '',
        columnas: [],
        nombre:''
    };
  datos: any;
    
constructor( public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { 
     this.fecha = JSON.parse(localStorage.getItem("fecha"));
    this.userbasic = JSON.parse(localStorage.getItem("usuario"));
    console.log(this.fecha);
  
}
    
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
      documento: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
      this.http.getdevice(this.urlgetcompra+this.fecha,this.userbasic.info).subscribe(data =>{
        this.tabladatos = data;
    });
        this.opciontabla();
    }
     opciontabla() {
       
         if(this.firstFormGroup.value.documento == "compras"){
                          console.log(this.urlgetventa+this.fecha);
             this.http.getdevice(this.urlgetcompra+this.fecha,this.userbasic.info).subscribe(data =>{
              this.datos = data;  
              if(this.datos.status != "Error" ){

              console.log(data);
                  Object.keys(this.datos.message).forEach((e) => {
                    this.tabladatos =  [];
                     const value = {
                        id : this.datos.message[e].id,
                        "Condición IVA" : this.datos.message[e].receiver,
                        Incluir: this.datos.message[e].receiver,
                        Tipo: this.datos.message[e].receiver,
                        "#Comprobante": this.datos.message[e].code,
                        "Cédula":this.datos.message[e].receiver,
                        Nombre: this.datos.message[e].sender,
                        Fecha: this.datos.message[e].date,
                        "Monto neto": this.datos.message[e].tax_base,
                        "Monto IVA": this.datos.message[e].taxt,
                        "Total Crédito":this.datos.message[e].total ,
                        "Total Gasto":this.datos.message[e].receiver,
                         "Acción Realizada":this.datos.message[e].receiver,
                         
                    };
         
         this.tabladatos.push(value);
                      console.log(this.tabladatos);
       });}
       this.envio = {
        datos: this.tabladatos,
        fecha: this.fecha,
        usuario: this.userbasic.info,
        columnas:['Condición IVA','Incluir','Tipo',"#Comprobante",'Cédula','Nombre','Fecha','Monto Neto','Monto IVA','Total Crédito','Total Gasto','Acción Realizada','Acciones'],
        nombre: "Compras"
    };
    this.ventas = false;
     localStorage.setItem("envio", JSON.stringify(this.envio)); 
             });
             setTimeout(() => {
               
          
              }, 200);
         }
         else{
             console.log(this.urlgetventa+this.fecha);
               this.http.getdevice(this.urlgetventa+this.fecha,this.userbasic.info).subscribe(data =>{
                this.datos = data;  
                if(this.datos.status != "Error" ){
                  Object.keys(this.datos.message).forEach((e) => {
                    this.tabladatos =  [];
                     const value = {
                      id : this.datos.message[e].id,
                        Tipo : this.datos.message[e].receiver,
                        "Actividad Económica": this.datos.message[e].receiver,
                        "#Comprobante": this.datos.message[e].code,
                        "Cédula":this.datos.message[e].receiver,
                        Nombre: this.datos.message[e].sender,
                        Fecha: this.datos.message[e].date,
                        "Monto neto": this.datos.message[e].tax_base,      
                        "Hacienda":this.datos.message[e].receiver ,
            
                         
                    };
         
                    this.tabladatos.push(value);
             });}
             this.envio = {
              datos: this.tabladatos,
              fecha: this.fecha,
              usuario: this.userbasic.info,
              columnas: ["Tipo","Actividad Económica","#Comprobante","Cédula","Nombre","Fecha","Monto Nto","Hacienda","Acciones"],
               nombre:"Ventas"
               
          };
          this.ventas = true;localStorage.setItem("envio", JSON.stringify(this.envio));
            });
           
         }
     }
    
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
