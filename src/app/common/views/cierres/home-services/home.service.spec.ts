import { TestBed } from '@angular/core/testing';

import { HomeServices } from './home.service';

describe('HomeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomeServices = TestBed.get(HomeServices);
    expect(service).toBeTruthy();
  });
});
