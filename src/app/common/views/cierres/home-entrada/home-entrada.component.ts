import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import { HomeIvaComponent } from '../home-iva/home-iva.component';
import { HomeRentaComponent } from '../home-renta/home-renta.component';
import { HomeAnualComponent } from '../home-anual/home-anual.component';

@Component({
	selector: 'app-entrada',
	templateUrl: './home-entrada.component.html',
	styleUrls: ['./home-entrada.component.scss'],

})
export class HomeEntradaComponent implements OnInit {

	fechaGroup: FormGroup;
	public url = environment.api+"/getCurrentEndOfMonth";
	
	userbasic;
	activo;
infos = {
    info : {
     	name:"",
     	start_date:"",
     	end_date:""

    }

    
  };
constructor( public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder, private route: ActivatedRoute) { 
	this.userbasic = JSON.parse(localStorage.getItem("usuario"));
	this.http.getdevice(this.url,this.userbasic.info).subscribe(data =>{
		this.activo = data;
		if(this.activo.status != "Error"){
			this.toastr.success("Carga Exitosa");
		}else{
			this.toastr.error(this.activo.message);
		}
	})
}
	
		ngOnInit() {
			this.fechaGroup = this._formBuilder.group({
				documento: ['', Validators.required],
				tipo :['',Validators.required]
			});
		
		}
	
		
	 
		onlyNumberKey(event) {
			return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
		}
		reloadComponent() {
			console.log('refrescado');
			const currentUrl = this.router.url;
			const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
			this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
		}
	}
