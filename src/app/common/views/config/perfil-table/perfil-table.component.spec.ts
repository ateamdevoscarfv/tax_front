import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilTableComponent } from './perfil-table.component';

describe('PerfilTableComponent', () => {
  let component: PerfilTableComponent;
  let fixture: ComponentFixture<PerfilTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
