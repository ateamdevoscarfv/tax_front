import { TestBed } from '@angular/core/testing';

import { PerfilServices } from './perfil.service';

describe('PerfilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PerfilServices = TestBed.get(PerfilServices);
    expect(service).toBeTruthy();
  });
});
