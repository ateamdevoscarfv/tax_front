import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { PerfilServices } from '../perfil-services/perfil.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MainContentComponent} from '../../../../root/main-content/main-content.component';

@Component({
  selector: 'app-perfil-form',
  templateUrl: './perfil-form.component.html',
  styleUrls: ['./perfil-form.component.scss']
})
export class PerfilFormComponent  implements OnInit  {

  firstFormGroup: FormGroup;
 
  public usuario;
  public userbasic;
    public userperfil;
    public ivas;
    public url = environment.api+"/updateUser/";
      public geturl = environment.api+"/getConfiguration/";
      public geturliva = environment.api+"/getTypesIvaSale";
  infos: {
		info: {
			 firstname: '',
        lastname : '',
       email: '',
        username  : '',
        rol  : '',
        password  : '',
        password_confirmation: ''    
		}
	};
constructor( public http: PerfilServices, public router: Router, public MainContent : MainContentComponent,
  public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
   this.usuario = JSON.parse(localStorage.getItem("user"));
      this.userbasic = JSON.parse(localStorage.getItem("usuario"));
      
      this.http.getdevice(this.geturl,this.userbasic.info).subscribe(data => {
          this.userperfil = data;
      }) ;
      this.http.getdevice(this.geturliva,this.userbasic.info).subscribe(data => {
        this.ivas = data;  
    }) 
}
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        firstname: ['', Validators.required],
        cuenta  : ['',Validators.required],
      
      });
    };
    actualizardatos(){
        console.log(this.userbasic.info);
        this.infos = {
		  info : {
			 firstname: this.firstFormGroup.value.firstname,
            lastname : this.firstFormGroup.value.lastname,
            email: this.firstFormGroup.value.email,
            username  : this.firstFormGroup.value.username,
            rol  : this.firstFormGroup.value.cuenta,
            password  : this.firstFormGroup.value.password,
            password_confirmation  : this.firstFormGroup.value.password   
		}
	};
        this.http.updatedevice(this.url+`${this.usuario.id}`, JSON.stringify(this.infos),this.userbasic.info).subscribe(data => {
            this.toastr.success("Perfil Actualizado con Exito");
            if(this.firstFormGroup.value.username != this.usuario.username){
                 localStorage.removeItem('usuario');
                localStorage.clear();
                this.router.navigateByUrl('login');
            }else{
            this.reloadComponent();
            }
           
        }, err =>{
            this.toastr.error("Error al Actualizar perfil");
        })
        
        
    }
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
