import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { HomeServices } from '../home-services/home.service';
import { HomeDialogDeleteComponent } from '../dialog/home-dialog-delete/home-dialog-delete.component';
import { HttpServices } from '../../../http/httpServices/httpServices';
import { environment } from '../../../../../environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-home-table',
  templateUrl: './home-table.component.html',
  styleUrls: ['./home-table.component.scss']
})
export class HomeTableComponent implements OnInit {
     tabladatos;
    element = [];
    public envio;

      dataSource;
      usuario;
      public url = environment.api+"/getReportsByYear/2019";
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(HomeDialogDeleteComponent) dynamic: HomeDialogDeleteComponent;
 

  contacto: FormGroup;

  nombre;
  fecha;
  expandedElement: null;

  columnsToDisplay = ["Nombre","Fecha","Tipo","Acciones"];

 // expandedElement: ClientElement | null;
  ClientElement = [];
  constructor(
    private HomeServices: HomeServices,
    public dialog: MatDialog, public router: Router,
    public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
     this.envio = JSON.parse(localStorage.getItem("envio"));
     this.usuario = JSON.parse(localStorage.getItem("usuario"));

    }
    

  ngOnInit() {
   
     this.loadDataTable();

  }

  public loadDataTable() {
    this.HomeServices.getdevice(this.url,this.usuario.info).subscribe(data =>{
        this.tabladatos = data;
        if(this.tabladatos.status != "Error"){
        this.element = [];
        Object.keys(this.tabladatos.message).forEach((e) =>{

           const VALOR = {
              "Nombre": this.tabladatos.message[e].name,
              "Fecha":  this.tabladatos.message[e].date,
              "Tipo":  this.tabladatos.message[e].type_report,
              urlpdf: this.tabladatos.message[e].url_pdf,
              urlexl: this.tabladatos.message[e].url_excel,
              urlxml: this.tabladatos.message[e].url_xml 
           };
           this.element.push(VALOR);

        })}else{
          this.toastr.error(this.tabladatos.message);
        }
        this.element.sort((a, b) => {
         const isAsc = 'asc';
         return this.compare(b.Nombre, a.Nombre, isAsc);
       })
       this.dataSource = new MatTableDataSource(this.element);
       this.dataSource.paginator = this.paginator;
       return this.dataSource; 
    })
       
 
   }

  public compare(a, b, isAsc) {
    return (a > b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public  CallLink(link) {
    window.open(link);
  }

  public openDialog(id, name) {
      /**
    this.HomeServices.getdevice(this.urlantena).subscribe(data =>{
      var datos = data;
       if (Object.keys(datos).length == 0){
        this.dialog.open(HomeDialogDeleteComponent, {
          width: '250px',
          data: {name: name, id: id}
        });
       }else{
         this.toastr.error("Este dispositivo esta activo en un acceso, no puede ser eliminado");
       }
    })
    console.log(id);*/
  
  }
  verBorrador(a){
         console.log(a);
      this.utils.CallLink(environment.report+"/"+a);
        
       
    }

 onlyNumberKey(event) {
  return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
reloadComponent(){
    console.log('refrescado');
    //hack para refrescar vista
    const currentUrl = this.router.url;
    const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
    this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
  }
  
}



