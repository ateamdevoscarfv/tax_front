import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeEntradaComponent } from './home-entrada.component';

describe('HomeEntradaComponent', () => {
  let component: HomeEntradaComponent;
  let fixture: ComponentFixture<HomeEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
