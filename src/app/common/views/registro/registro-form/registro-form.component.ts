import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { RegistroServices } from '../registro-services/registro.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-registro-form',
  templateUrl: './registro-form.component.html',
  styleUrls: ['./registro-form.component.scss']
})
export class RegistroFormComponent implements OnInit {

  firstFormGroup: FormGroup;
  public url = environment.api+"/register";
public resp;
env : {
    info: {
    firstname : '',
    lastname : '',
    email : '',
    username : '',
    password:'',
    password_confirmation: '' }  
  };
constructor( public http: RegistroServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { }
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        firstname: ['', Validators.required],
        lastname: ['',Validators.required],
        email: ['',[Validators.required, Validators.email]],
        username: ['',Validators.required],
        password: ['',Validators.required],
        password_confirmation: ['',Validators.required]   
      }, {
            validator: this.MustMatch('password', 'password_confirmation')
        });
    
    }
   enviar(){
       this.env = {
            info: {
                firstname : this.firstFormGroup.value.firstname,
                lastname : this.firstFormGroup.value.lastname,
                email : this.firstFormGroup.value.email,
                username : this.firstFormGroup.value.username,
                password:this.firstFormGroup.value.password,
                password_confirmation:this.firstFormGroup.value.password_confirmation }  
  };
       setTimeout(() => { 
       this.http.postdevice(this.url,JSON.stringify(this.env)).subscribe(data => {
           this.resp = data;
            if(this.resp.status == "Success"){
               this.toastr.success("Registro Exitoso");
                this.toastr.success("Al ingresar Completar Su Informacion en el apartado Perfil");
               
             this.router.navigateByUrl('login');    
            }else{
               ;
                   
            }
       }, error => {
           let res = error;
                if(res.error.message.clie_password != undefined){
                this.toastr.error(res.error.message.clie_password[0]);}
            if(res.error.message.clie_email != undefined){
                 this.toastr.error(res.error.message.clie_email[0]);}
            if(res.error.message.clie_username != undefined){
                this.toastr.error(res.error.message.clie_username[0]);}
           
       })
       }, 300);   
   } 

    atras(){
        this.router.navigateByUrl('login');  
    }
    MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
