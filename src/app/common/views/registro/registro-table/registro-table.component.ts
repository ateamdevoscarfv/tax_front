import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { RegistroServices } from '../registro-services/registro.service';
import { RegistroDialogDeleteComponent } from '../dialog/registro-dialog-delete/registro-dialog-delete.component';
import { RegistroFormComponent } from '../registro-form/registro-form.component';
import { HttpServices } from '../../../http/httpServices/httpServices';
import { environment } from '../../../../../environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-registro-table',
  templateUrl: './registro-table.component.html',
  styleUrls: ['./registro-table.component.scss']
})
export class RegistroTableComponent implements OnInit {


  dispotiposn: any;
  dispotipo: any;
  abridor= [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(RegistroDialogDeleteComponent) dynamic: RegistroDialogDeleteComponent;
  @ViewChild(RegistroFormComponent) form: RegistroFormComponent;

  contacto: FormGroup;
  submitted = false;
  blurEmail: boolean = false;
  dataSource;
  add: boolean = false;
  listar: boolean = true;
  email: any;
  addactive: boolean = false;
  listactive: boolean = true;
  element: any;
  dataFilter: any;
  endpointReport: any;
  impLink: string;
  dataEdit: any;
  
  expandedElement: null;
  identificador: any;
  firstFormGroup: FormGroup;
  idr: any;
  variable: any;
  dispot;
  @Input() ver;
  @Input() row;
  @Input() img;
  @Input() nombrer = '';
  @Input() descripcionr: string = '';
  @Input() ipr: string  = '0.0.0.0';
  @Input() numeror: any;
  @Input() userm: any;
  @Input() passm: any;
  @Input() puertor: number = 0;
  @Input() deviceTyper;

  columnsToDisplay = ['Nombre', 'Descripción','Dirección URL','Número','Acciones'];
  env = {
    deviceType : '',
    name : '',
    descr : '',
    identifier : '',
    url : '',
    number : 0,
    autoCloseSeconds: 10
  };
  arrantena = ['opener','openerde2','opener2','openerde8','opener8'];
public urlantena = environment.api+'/in';

 // expandedElement: ClientElement | null;
  ClientElement = [];
  constructor(
    private RegistroServices: RegistroServices,
    public dialog: MatDialog, public router: Router,
    public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
  
     
    }
    

  ngOnInit() {
   
  }
  public loadDataTable() {
        
     
 
   }

  public compare(a, b, isAsc) {
    return (a > b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public  CallLink(link) {
    window.open(link);
  }

  public openDialog(id, name) {
    this.RegistroServices.getdevice(this.urlantena).subscribe(data =>{
      var datos = data;
       if (Object.keys(datos).length == 0){
        this.dialog.open(RegistroDialogDeleteComponent, {
          width: '250px',
          data: {name: name, id: id}
        });
       }else{
         this.toastr.error("Este dispositivo esta activo en un acceso, no puede ser eliminado");
       }
    })
    console.log(id);
  
  }


 onlyNumberKey(event) {
  return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
  reloadComponent(){
    console.log('refrescado');
    //hack para refrescar vista
    const currentUrl = this.router.url;
    const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
    this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
  }
}


