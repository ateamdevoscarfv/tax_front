import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { UtilsService } from '../../../utils/services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { RegistroModel } from '../registro-models/registro.interface';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RegistroServices {
 constructor(
   public http: HttpClient
 ) { }  

  getdevice(url) {
    return this.http.get(url);
}
postdevice(url, env) {
    return this.http.post(url, env);
}
deletedevice(url) {
   return this.http.delete(url);
}
updatedevice(url, env) {
   return this.http.put(url, env);
}
}