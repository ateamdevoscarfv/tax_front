import { TestBed } from '@angular/core/testing';

import { RegistroServices } from './registro.service';

describe('RegistroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegistroServices = TestBed.get(RegistroServices);
    expect(service).toBeTruthy();
  });
});
