import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistroRoutingModule } from './registro-routing.module';
import { RegistroComponent } from './registro.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MaterialModule } from '../../../material/material';
import { RegistroTableComponent } from './registro-table/registro-table.component';
import { RegistroFormComponent } from './registro-form/registro-form.component';
import { RegistroDialogDeleteComponent } from './dialog/registro-dialog-delete/registro-dialog-delete.component';
import { RegistroServices } from './registro-services/registro.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [RegistroComponent, RegistroTableComponent,
    RegistroDialogDeleteComponent, RegistroFormComponent
    ],
  imports: [
    CommonModule,
    RegistroRoutingModule,
    MDBBootstrapModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ RegistroServices ],
  entryComponents: [ RegistroDialogDeleteComponent ]
})
export class RegistroModule { }
