import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 
import { tablereload } from '../home-table/home-table.component';
import { HomeIvaSevice } from '../home-iva/home-iva.service.';
var imprime;
@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent  implements OnInit  {


  firstFormGroup: FormGroup;
    public url = environment.api+"/saveSpecialProrata";
  public urlespecial = environment.api+"/saveSpecialProrata";
  public urlgenera = environment.api+"/saveGeneralProrata";
  public imprime2; 
  public userbasic;
 public pror;
public cardcal = false;
  infos = {
    info : {
        type_report : "",
        number_of_files : "",
        files : {
        
            file:{
                type_file: "",
                xml: ""
            }
        }
    }

    
  };
  cardespecial: boolean = false;
  res;
  prorata1: number;
  prorata2: number;
  prorata3: number;
  prorata4: number;
  ventaCredito: number;
  ventaScrediro: number;

constructor(public homeIva: HomeIvaSevice, private table: tablereload, public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { }
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        periodo: ['', Validators.required],
        prorata: ['',Validators.required],
        general: [''],
        excentas: [''],
        excentac: [''],
        gravadas: [''],
           pror: [''],
           especial:[''],
          oneporcentage:[''],
           twoporcentage:[''],
           treeporcentage:[''],
           fourporcentage:[''],
           treceporcentage:[''],
           ventas1:[''],
           ventas2:[''],
           ventas4:[''],
           ventas13:[''],
        
        
      });
        this.userbasic = JSON.parse(localStorage.getItem("usuario"));
    }

    vercal(){
        this.cardcal = !this.cardcal;

    }
    vercalespecial(){
       this.cardespecial = !this.cardespecial;
    }
    calcular(){
      let derchoc = parseFloat( this.firstFormGroup.value.gravadas)  + parseInt(this.firstFormGroup.value.excentac);
      let operaciones = parseFloat( this.firstFormGroup.value.gravadas)  + parseInt(this.firstFormGroup.value.excentac)+parseFloat(this.firstFormGroup.value.excentas) ; 
        let general = derchoc / operaciones;
      
        this.pror = general*100;
    }
    calcularSpecial(){
      let TotalVentas = parseFloat(this.firstFormGroup.value.ventas1)+parseFloat(this.firstFormGroup.value.ventas2)+parseFloat(this.firstFormGroup.value.ventas4)+parseFloat(this.firstFormGroup.value.ventas13)+
      parseFloat(this.firstFormGroup.value.excentac)+parseFloat(this.firstFormGroup.value.excentas);
       
      this.prorata1 = (parseFloat(this.firstFormGroup.value.ventas1)/TotalVentas)*100;
      this.prorata2= (parseFloat(this.firstFormGroup.value.ventas2)/TotalVentas)*100;
      this.prorata3= (parseFloat(this.firstFormGroup.value.ventas4)/TotalVentas)*100;
      this.prorata4= (parseFloat(this.firstFormGroup.value.ventas13)/TotalVentas)*100;
      this.ventaCredito =(parseFloat(this.firstFormGroup.value.excentac)/TotalVentas)*100;
      this.ventaScrediro =(parseFloat(this.firstFormGroup.value.excentas)/TotalVentas)*100;

   
    }
    guardar(){
        if(this.firstFormGroup.value.prorata == "1"){
            if(this.cardcal == false){ 
                let env = {
                    info : {
                        prorata_general: parseFloat(this.firstFormGroup.value.general) 
                
            }
        };
                this.http.postdevice(this.urlgenera, JSON.stringify(env), this.userbasic.info).subscribe(data =>{
                  this.res = data;
                  if(this.res.status == "Success")  {
                  this.toastr.success(this.res.message);
                  this.reloadComponent();
                }
                  else{
                    this.toastr.error(this.res.message);
                    this.reloadComponent();
                  }
                })
                ;}else{
                 let env = {
                    info : {
                        prorata_general: this.pror 
                
            }
        };  this.http.postdevice(this.urlgenera, JSON.stringify(env), this.userbasic.info).subscribe(data =>{
          this.res = data;
          if(this.res.status == "Success")  {
          this.toastr.success(this.res.message);
          this.reloadComponent();
        }
          else{
            this.toastr.error(this.res.message);
            this.reloadComponent();
          }
        });
            }
            
           
            
        }else{
          if(this.cardespecial == true){ 
               let env = {
                    info : {
                        prorata_one_percent: this.prorata1,
                        prorata_two_percent: this.prorata2,
                        prorata_four_percent: this.prorata3,
                        prorata_thirteen_percent: this.prorata4,
                        prorata_exempt_with_credit: this.ventaCredito,
                        prorata_exempt_without_credit: 100- (this.prorata1+this.prorata2+this.prorata3+this.prorata4+this.ventaCredito)
                
                }
             };  this.http.postdevice(this.urlespecial, JSON.stringify(env), this.userbasic.info).subscribe(data =>{
              this.res = data;
              if(this.res.status == "Success")  {
              this.toastr.success(this.res.message);
              this.reloadComponent();
            }
              else{
                this.toastr.error(this.res.message);
                this.reloadComponent();
              }
            })
        }else{
          let env = {
            info : {
                prorata_one_percent: this.firstFormGroup.value.oneporcentage,
                prorata_two_percent: this.firstFormGroup.value.twoporcentage,
                prorata_four_percent: this.firstFormGroup.value.fourporcentage,
                prorata_thirteen_percent: this.firstFormGroup.value.treceporcentage,
                prorata_exempt_with_credit: this.firstFormGroup.value.especial,
                prorata_exempt_without_credit: 100-(parseFloat(this.firstFormGroup.value.oneporcentage)+parseFloat(this.firstFormGroup.value.twoporcentage)
                +parseFloat(this.firstFormGroup.value.fourporcentage)+parseFloat(this.firstFormGroup.value.treceporcentage)+parseFloat(this.firstFormGroup.value.especial))
                
        
            }
          };  this.http.postdevice(this.urlespecial, JSON.stringify(env), this.userbasic.info).subscribe(data =>{
               this.res = data;
             if(this.res.status == "Success")  {
             this.toastr.success(this.res.message);
            this.reloadComponent();
          }
             else{
               this.toastr.error(this.res.message);
              this.reloadComponent();
      }
    })
        }
      
      }
       
    }
  
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
}