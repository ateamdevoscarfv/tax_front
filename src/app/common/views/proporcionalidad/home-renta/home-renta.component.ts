import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-renta',
  templateUrl: './home-renta.component.html',
  styleUrls: ['./home-renta.component.scss']
})
export class HomeRentaComponent implements OnInit {

    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    tercerFormGroup: FormGroup;
    cuartoFormGroup: FormGroup;
  env = {
    deviceType : '',
    name : '',
    descr : '',
    identifier : '',
    url : '',
    number : 0,
    autoCloseSeconds: 10
    
  }; 
constructor( public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { }
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
       this.tercerFormGroup = this._formBuilder.group({
      tercerCtrl: ['', Validators.required]
    });
    this.cuartoFormGroup = this._formBuilder.group({
      cuartoCtrl: ['', Validators.required]
    });
    }
 
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
