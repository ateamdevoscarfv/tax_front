import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import { HomeIvaComponent } from '../home-iva/home-iva.component';
import { HomeRentaComponent } from '../home-renta/home-renta.component';
import { HomeAnualComponent } from '../home-anual/home-anual.component';
// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';

// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
export interface Food {
  value: string;
  viewValue: string;
};


@Component({
  selector: 'app-entrada',
  templateUrl: './home-entrada.component.html',
  styleUrls: ['./home-entrada.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class HomeEntradaComponent implements OnInit {
public fecha = "" ;   
tramite = "iva";
  public contenido = true;   
public iva = false;
public renta = false;
public anual = false;
  fechaGroup: FormGroup;
  foods: Food[] = [
    {value: 'iva', viewValue: 'Declaración del Iva'},
    {value: 'anual', viewValue: 'Declaración Anual de Clientes y Proveedores'},
    {value: 'renta', viewValue: 'Declaración de Renta'}
  ];  
date = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }
public urlprorata = environment.api+"/getProportionality";
 prorrata = true;
 protr;
 verificado;
  env = {
    deviceType : '',
    name : '',
    descr : '',
    identifier : '',
    url : '',
    number : 0,
    autoCloseSeconds: 10
    
  }; 
  userbasic;
constructor( public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder, private route: ActivatedRoute) { 
this.userbasic = JSON.parse(localStorage.getItem("usuario"));
}
  
    ngOnInit() {
      this.fechaGroup = this._formBuilder.group({
        documento: ['', Validators.required],
        tipo :['',Validators.required]
      });
     this.http.getdevice(this.urlprorata,this.userbasic.info).subscribe(data => {
          this.verificado = data;
          if(this.verificado.message.type_prorata == "special"){
            this.prorrata = true;
            this.protr = data;
          }else{
            this.prorrata = false;
             this.protr = data;
          }
         
      }) 


    }
    vefecha(){
        let a;
        var esp = " ";
        a = this.date.value._d;
        var b = JSON.stringify(a); 
        var c = b.split("-",2);
        var k = c[0].slice(1);
        var z = c[1]
        this.fecha = z+"/"+k;
        localStorage.setItem("fecha", JSON.stringify(this.fecha));
        
        }

    comenzarborradorD1(){
        if(this.fechaGroup.value.documento == 'iva' ){
            
             this.contenido = false;
            this.iva = true;
            this.anual = false;
            this.renta = false;
        } 
         if(this.fechaGroup.value.documento == 'anual' ){
             this.contenido = false;
            this.iva = false;
            this.anual = true;
            this.renta = false;
            
        } 
         if(this.fechaGroup.value.documento == 'renta' ){
            
            this.contenido = false;
            this.iva = false;
            this.anual = false;
            this.renta = true; 
        } 
       
    }
    volver(){
        this.contenido = true;
            this.iva = false;
            this.anual = false;
            this.renta = false; 
       
    }
   
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
