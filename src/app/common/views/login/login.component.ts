import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoginServices } from './login-services/login-services';
import { loginMsg } from '../../utils/const/message';
import { environment } from '../../../../environments/environment';

declare const $: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    firstFormGroup: FormGroup;
   public resp;
   infos: {
		info: {
			user: "",
			pass: ""
		}
	};
    env = {
      name : '',
      pass : ''
      
      
    };
    public url = environment.api+"/login";
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private LoginServices: LoginServices,
        public toastr: ToastrService,
        private router: Router,
        public fb: FormBuilder
    ) {
        this.firstFormGroup = this.formBuilder.group({
            name: ['', Validators.required],
            pass: ['',Validators.required]
          });
        
    }
   
    ngOnInit() {
     
    }
    entrar(){
      
          this.infos = {
              info: {
			user: this.firstFormGroup.value.name,
			pass: this.firstFormGroup.value.pass
		}
          }
          setTimeout(() => {
          this.LoginServices.postdevice(this.url, JSON.stringify(this.infos)).subscribe( data => {
              this.resp = data;
                      if(this.resp.status == "Success"){
              localStorage.setItem("usuario", JSON.stringify(this.infos));
                          localStorage.setItem("user", JSON.stringify(this.resp.user));
            this.router.navigateByUrl('perfil');}
                      else{
                        this.toastr.error(this.resp.message);  
                      }
          }, err => {
            this.toastr.error("Error Interno del Servidor");
                    })
          }, 300);   
      }
  

 
    isInArray(value, array) {
        return array.indexOf(value) > -1;
    }

    checkFullPageBackgroundImage() {
        var $page = $('.full-page');
        var image_src = $page.data('image');
        if(image_src !== undefined) {
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    }
}
