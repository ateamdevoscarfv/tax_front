import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
//mport { HttpServicesLogin } from '../../../http/httpServices/httpServicesLogin';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoginServices {
     constructor(
       public http: HttpClient
     ) { }  
    
      getdevice(url) {
        return this.http.get(url);
    }
    postdevice(url, env) {
        return this.http.post(url, env);
    }
    deletedevice(url) {
       return this.http.delete(url);
    }
    updatedevice(url, env) {
       return this.http.put(url, env);
    }
    }
