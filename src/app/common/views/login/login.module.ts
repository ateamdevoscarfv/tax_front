import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../../material/material';
import { ParticlesModule } from 'angular-particle';

import {
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule
} from '@angular/material';

import { LoginServices } from './login-services/login-services';

@NgModule({
  declarations: [LoginComponent],
  imports: [
      CommonModule,
      LoginRoutingModule,
      ReactiveFormsModule,
      FormsModule,
      MaterialModule,
      MatFormFieldModule,
      MatInputModule,
      MatMenuModule,
      MatPaginatorModule,
      MatSortModule,
      MatTableModule,
      ParticlesModule
  ],
    providers: [
        LoginServices
    ]
})
export class LoginModule { }
