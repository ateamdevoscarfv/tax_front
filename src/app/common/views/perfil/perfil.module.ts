import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { PerfilComponent } from './perfil.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MaterialModule } from '../../../material/material';
import { PerfilTableComponent } from './perfil-table/perfil-table.component';
import { PerfilFormComponent } from './perfil-form/perfil-form.component';
import { PerfilDialogDeleteComponent } from './dialog/perfil-dialog-delete/perfil-dialog-delete.component';
import { PerfilServices } from './perfil-services/perfil.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PerfilComponent, PerfilTableComponent,
    PerfilDialogDeleteComponent, PerfilFormComponent
    ],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    MDBBootstrapModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ PerfilServices ],
  entryComponents: [ PerfilDialogDeleteComponent ]
})
export class PerfilModule { }
