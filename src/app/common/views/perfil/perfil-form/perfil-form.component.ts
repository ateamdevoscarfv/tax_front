import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { PerfilServices } from '../perfil-services/perfil.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MainContentComponent} from '../../../../root/main-content/main-content.component';

@Component({
  selector: 'app-perfil-form',
  templateUrl: './perfil-form.component.html',
  styleUrls: ['./perfil-form.component.scss']
})
export class PerfilFormComponent  implements OnInit  {

  firstFormGroup: FormGroup;
 
  public usuario;
  public userbasic;
  public userperfil;
  public url = environment.api+"/updateUser/";
  public geturl = environment.api+"/getUser/";
  public geturlpais = environment.api+"/getCountries";
  public geturlcedula = environment.api;
  public geturlProvinces = environment.api+"/getProvinces";
  public geturlCantons = environment.api+"/getCantons";
  public geturlDistricts = environment.api+"/getDistricts";
  pais ;canton;distrito;cedula;provincia;
  infos: {
		info: {
	             firstname: "",
            lastname : "",
            dni: "",
            phone_number: "",
            email: "",
            username  : "",
            password  : "",
            password_confirmation  : "",
            role  : 2,
            type_document_id  : "",
            name_business  : "",
            legal_dni  : "",
            country  : "",
            province  : "",
            canton  : "",
            district  : "",
            address  : "",
            
		}
	};
constructor( public http: PerfilServices, public router: Router, public MainContent : MainContentComponent,
  public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
   this.usuario = JSON.parse(localStorage.getItem("user"));
      this.userbasic = JSON.parse(localStorage.getItem("usuario"));
         
      this.http.getdevice(this.geturl+`${this.usuario.id}`,this.userbasic.info).subscribe(data => {
          this.userperfil = data;
          console.log(this.userperfil);
      }) ;
      this.http.getdevice(this.geturlpais,this.userbasic.info).subscribe(data => {
          this.pais = data;
          console.log(this.userperfil);
      }) ;
      
}
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        firstname: ['', Validators.required],
        lastname : ['',Validators.required],
        identification: ['', Validators.required],
        telephone: ['', Validators.required],
        email: ['',[Validators.required, Validators.email]],
        username  : ['',Validators.required],
        password  : ['',Validators.required],
        password_confirmation  : ['',Validators.required],
        rol  : ['',Validators.required],
        type_person  : ['',Validators.required],
        trade_name  : ['',Validators.required],
        legal_number  : ['',Validators.required],
        country  : ['',Validators.required],
        province  : ['',Validators.required],
        canton  : ['',Validators.required],
        district  : ['',Validators.required],
        exact_direction  : ['',Validators.required],
        tipopersona  : ['',Validators.required]
         
      });
        
    this.provincia = [];this.canton= [];this.distrito =[];
    
    };
    llamarProvincia(){
    this.canton= [];this.distrito =[];
      this.http.getdevice(this.geturlProvinces +"/"+this.firstFormGroup.value.country,this.userbasic.info).subscribe(data =>{
        this.provincia = data;
      })
    }
    llamarCanton(){
    this.distrito =[];
      this.http.getdevice(this.geturlCantons+"/"+this.firstFormGroup.value.country+"/"+this.firstFormGroup.value.province,this.userbasic.info).subscribe(data =>{
        this.canton = data;
      })
    }
    llamarDistrito(){
  
      this.http.getdevice(this.geturlDistricts+"/"+this.firstFormGroup.value.country+"/"+this.firstFormGroup.value.province+"/"+this.firstFormGroup.value.canton,this.userbasic.info).subscribe(data =>{
        this.distrito = data;
      })
    }
    actualizardatos(){
        console.log(this.userbasic.info);
        this.infos = {
		  info : {
			      firstname: this.firstFormGroup.value.firstname,
            lastname : this.firstFormGroup.value.lastname,
            dni: this.firstFormGroup.value.identification,
            phone_number: this.firstFormGroup.value.telephone,
            email: this.firstFormGroup.value.email,
            username  : this.firstFormGroup.value.username,
            password  : this.firstFormGroup.value.password,
            password_confirmation  : this.firstFormGroup.value.password_confirmation,
            role  : 2,
            type_document_id  : this.firstFormGroup.value.tipopersona,
            name_business  : this.firstFormGroup.value.trade_name,
            legal_dni  : this.firstFormGroup.value.legal_number,
            country  : this.firstFormGroup.value.country,
            province  : this.firstFormGroup.value.province,
            canton  : this.firstFormGroup.value.canton,
            district  : this.firstFormGroup.value.district,
            address  : this.firstFormGroup.value.exact_direction,
            
    }
  };
        console.log(this.infos);
        this.http.updatedevice(this.url+`${this.usuario.id}`, JSON.stringify(this.infos),this.userbasic.info).subscribe(data => {
          this.toastr.success("Perfil Actualizado con Exito");
             if(this.firstFormGroup.value.username != this.usuario.username){
                  localStorage.removeItem('usuario');
                 localStorage.clear();
                 this.router.navigateByUrl('login');
            }else{
              this.reloadComponent();
             }
           
         }, err =>{
             this.toastr.error("Error al Actualizar perfil");
         })
        
        
    }
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
