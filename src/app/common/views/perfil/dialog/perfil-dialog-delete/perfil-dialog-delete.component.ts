import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { PerfilServices } from '../../perfil-services/perfil.service';
import { ToastrService } from 'ngx-toastr';
import { UtilsService } from '../../../../utils/services/utils.service';
import { devicesMsg  } from '../../../../utils/const/message';
import { environment } from '../../../../../../environments/environment';

@Component({
  selector: 'app-client-dialog-delete',
  templateUrl: './perfil-dialog-delete.component.html',
  styleUrls: ['./perfil-dialog-delete.component.scss']
})
export class PerfilDialogDeleteComponent implements OnInit {
  public urldispo = environment.api+'/in';
  public name: string ;
  public id;
  constructor(
    public dialogRef: MatDialogRef<PerfilDialogDeleteComponent>,
    public snackBar: MatSnackBar,
    public router: Router,
    private PerfilServices: PerfilServices,
    private toastr: ToastrService,
    private utils: UtilsService,
  
    @Inject(MAT_DIALOG_DATA) public data: MatDialog) {
      this.name = data['name'];
      this.id = data['id'];
    }

  public deleteRow(id) {
  /**
    this.AbridorServices.deletedevice(this.urldispo+'/'+id).subscribe(data => {
      console.log(data);  
      this.reloadComponent();
      this.toastr.success(devicesMsg.delete);
    
    },
      error => {
        this.toastr.error(devicesMsg.delete);
      });*/
  }

    openSnackBar(message: string, action: string) {
      this.snackBar.open(message, action, {
        duration: 2000,
      });
    }

  ngOnInit() {
  }
    reloadComponent() {
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }

}
