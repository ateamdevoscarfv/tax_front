import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { PerfilServices } from '../perfil-services/perfil.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MainContentComponent} from '../../../../root/main-content/main-content.component';


import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-perfil-form',
  templateUrl: './perfil-form.component.html',
  styleUrls: ['./perfil-form.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class PerfilFormComponent  implements OnInit  {
 prorrata = true;
  date = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }
  firstFormGroup: FormGroup;
  fecha;
  respuesta;
  mes; year;
  public usuario;
  public userbasic;
  public userperfil;
  protr;
  public urlprorata = environment.api+"/getProportionality";
  public urlresumen = environment.api+"/setSummaryOfMonth";
    public url = environment.api+"/updateUser/";
      public geturl = environment.api+"/getUser/";
  infos: {
		info: {
			 year: '',
        month : ''
      
		}
	};
constructor( public http: PerfilServices, public router: Router, public MainContent : MainContentComponent,
  public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
   this.usuario = JSON.parse(localStorage.getItem("user"));
      this.userbasic = JSON.parse(localStorage.getItem("usuario"));
      

}
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        firstname: ['', Validators.required],
        lastname : ['',Validators.required],
       email: ['',[Validators.required, Validators.email]],
        username  : ['',Validators.required],
        cuenta  : ['',Validators.required],
        password  : ['',Validators.required],
        a : ['',Validators.required],
        b : ['',Validators.required],
        c : ['',Validators.required],
        d : ['',Validators.required],
        m : ['',Validators.required],
        n : ['',Validators.required],
        o : ['',Validators.required],
        p : ['',Validators.required],
        q : ['',Validators.required],
        w : ['',Validators.required],
      });
 this.http.getdevice(this.urlprorata,this.userbasic.info).subscribe(data => {
          this.protr = data;
          if(this.protr.message.type_prorata == "special"){
            this.prorrata = true;
          }else{
            this.prorrata = false;
          }
         this.actualizardatos();
      }) 
   
    
    };
      vefecha(){
    let a;
    var esp = " ";
    a = this.date.value._d;
    var b = JSON.stringify(a); 
    var c = b.split("-",2);
    var k = c[0].slice(1);
    var z = c[1]
    this.fecha = z+"/"+k;
    this.mes = z;
    this.year = k;
       this.infos = {
        info: {
          year: this.year,
          month:this.mes
        }
    };
   // this.loadDataTable();
    }
    actualizardatos(){
      this.vefecha();
      this.http.postdevice(this.urlresumen,JSON.stringify(this.infos),this.userbasic.info).subscribe(data => {
       this.respuesta = data;
       if(this.respuesta.status != "Error" ){
        this.toastr.success(this.respuesta.message);

      }
        else{
          this.toastr.error(this.respuesta.message)
        }
      })
        
    }
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
