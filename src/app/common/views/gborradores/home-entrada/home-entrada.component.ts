import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import { HomeIvaComponent } from '../home-iva/home-iva.component';
import { HomeRentaComponent } from '../home-renta/home-renta.component';
import { HomeAnualComponent } from '../home-anual/home-anual.component';
// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';

// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
export interface Food {
  value: string;
  viewValue: string;
};


@Component({
  selector: 'app-entrada',
  templateUrl: './home-entrada.component.html',
  styleUrls: ['./home-entrada.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class HomeEntradaComponent implements OnInit {
public fecha = "" ;   
tramite = "1";
  public contenido = true;   

fechaGroup: FormGroup;
year; month;
urlreporte = environment.api+"/generateReport";
impLink;
userbasic;res;
foods: Food[] = [
    {value: '1', viewValue: 'Declaración del Iva'},
    {value: '3', viewValue: 'Declaración Anual de Clientes y Proveedores'},
    {value: '2', viewValue: 'Declaración de Renta'}
  ];  
date = new FormControl(moment());

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }

  
constructor( public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder, private route: ActivatedRoute) { 
  this.userbasic = JSON.parse(localStorage.getItem("usuario"));
}
  
    ngOnInit() {
      this.fechaGroup = this._formBuilder.group({
        documento: ['', Validators.required],
        tipo :[''],
        tiempo: ['']
      });
    
    }
    vefecha(){
        let a;
        var esp = " ";
        a = this.date.value._d;
        var b = JSON.stringify(a); 
        var c = b.split("-",2);
        var k = c[0].slice(1);
        var z = c[1]
        this.fecha = z+"/"+k;
        this.year = z;
        this.month = k;
        
        }

  generarBorradorDPF(){
    this.vefecha();
    let env = {
        info : {
          month: this.year,
          year: this.month,
          type : this.fechaGroup.value.documento,
          confirm: ""

        }
      }
    
      this.http.postdevice(this.urlreporte,env,this.userbasic.info).subscribe(data => {
       this.res = data;
        
          if(this.res.status != "Error"){
            this.contenido = false;
          const body = data['message'];
          this.impLink = environment.report+"/"+this.res.message;
          this.utils.CallLink(this.impLink);}
          else{
            this.toastr.error(this.res.message)
          }
        
        },error =>{
            this.toastr.error("Error al Crear el Reporte")
        });
       
    }
   confirmar(a){
         let env = {
        info : {
          month: this.year,
          year: this.month,
          type : this.fechaGroup.value.documento,
          confirm: a

        }
      }
      this.http.postdevice(this.urlreporte,env,this.userbasic.info).subscribe(data =>{
        this.res = data;
        this.toastr.success(this.res.message);
        this.reloadComponent() ;

      })
    

      }
    /**
    generarBorradorEXEL(){
    let env = {
      title: "Abridores",
      columns:  ['Nombre', 'Descripción','Dirección URL','Número'],
      elements: this.element,
      filename: "Abridores.pdf"
      }
    
      this.http.postdevice(this.urlreportepdf,env,this.userbasaic.info).subscribe(data => {
        if (data) {
          const body = data['message'];
          this.impLink = environment.apidevice+'mng/v1/public/reports/Reporte.pdf';
          this.utils.CallLink(this.impLink);
        }
        },error =>{
            this.toastr.error("Error al Crear el Reporte")
        });
       
    }
    generarBorradorXML(){
    let env = {
      title: "Abridores",
      columns:  ['Nombre', 'Descripción','Dirección URL','Número'],
      elements: this.element,
      filename: "Abridores.pdf"
      }
    
      this.http.postdevice(this.urlreportepdf,env,this.userbasaic.info).subscribe(data => {
        if (data) {
          const body = data['message'];
          this.impLink = environment.apidevice+'mng/v1/public/reports/Reporte.pdf';
          this.utils.CallLink(this.impLink);
        }
        },error =>{
            this.toastr.error("Error al Crear el Reporte")
        });
       
    }*/
   
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
