import { Component, OnInit, ViewChild, Input,Output } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { HomeServices } from '../home-services/home.service';
import { HomeDialogDeleteComponent } from '../dialog/home-dialog-delete/home-dialog-delete.component';
import { HttpServices } from '../../../http/httpServices/httpServices';
import { environment } from '../../../../../environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ErrorStateMatcher} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';

// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-home-table',
  templateUrl: './home-table.component.html',
  styleUrls: ['./home-table.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HomeTableComponent implements OnInit {
    TotalVentaNeta;
    TotalIVA;
    TotalVenta;
    IVASoportado;
    IVADeducible;
    TotalGravada;
    Gasto;
    IVA13; IVA2;
    cont = 0;
prorrata = false;
  date = new FormControl(moment());
     tabladatos;
    element = [];
    public envio;
      dataSource;

      usuario;
      datos;
      public urldelete = environment.api+"/deleteInvoice/"
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(HomeDialogDeleteComponent) dynamic: HomeDialogDeleteComponent;
 
  columnsToDisplay=['Actividad Economica','N° Factura','Fecha de la Factura',"Fecha de Ingreso",'Cédula','Nombre','Venta Neta','IVA Total','Venta Total Neta','Gravadas 13%','Gravadas 1%','Con Derecho','Sin Derecho','Acciones'];
  columnsTo= [];
  elementos = [];
displayedColumns;
  contacto: FormGroup;
respo;
  nombre;
  fecha;
  expandedElement: null;
infos = {
    info : {
      user:"",
      pass:"",
         type_invoice:""
    }

    
  };

public url = environment.api+"/getSales/"; ;

 // expandedElement: ClientElement | null;
  ClientElement = [];
  userbasic: any;
  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }
  constructor(
    private http: HomeServices,
    public dialog: MatDialog, public router: Router,
    public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
      this.userbasic = JSON.parse(localStorage.getItem("usuario"));
    }
    

  ngOnInit() {
    //this.verprorrata("general");
      this.loadDataTable();
 
  }
  delete(id,type){
    this.infos = {
        info: {
          user:this.userbasic.info.user,
          pass:this.userbasic.info.pass,
          type_invoice: type
        }
    }
    console.log(this.infos);
    this.http.deletedevice(this.urldelete+id+'/'+type,this.userbasic.info).subscribe(data =>{
      console.log("heders")
      console.log(this.infos.info);
       this.respo = data;
      if(this.respo.status == "Error"){
        this.toastr.error("Error al eliminar")
      }else{
          this.toastr.success("Documento Eliminado")
      this.loadDataTable();
      }
    
    },err => {
      this.toastr.error("Error al eliminar")
    }
    )
  }


loadDataTable() {
    
      this.datos= [];
       this.fecha="";
       this.vefecha();
       
       setTimeout(() => {
    this.http.getdevice(this.url+this.fecha,this.userbasic.info).subscribe(data =>{
     
      this.datos = data;  
        console.log(this.datos);
      if(this.datos.status != "Error" ){
          this.TotalVentaNeta = 0;
          this.TotalIVA = 0;
          this.TotalVenta = 0;
          this.IVASoportado = 0
          this.IVADeducible = 0;
          this.Gasto = 0
           this.IVA13 = 0; this.IVA2 = 0;
          this.element=[];
          Object.keys(this.datos.message).forEach((e) => {
             const value = {
                id : this.datos.message[e].id,
                "Actividad Economica" : this.datos.message[e].activity_code,
                "N° Factura": this.datos.message[e].code,
                "Fecha de la Factura": this.datos.message[e].date,
                "Fecha de Ingreso": this.datos.message[e].upload_date,
                "Cédula":this.datos.message[e].document_number_id,
                Nombre: this.datos.message[e].client,
                "Venta Neta": this.datos.message[e].tax,
                "IVA Total": this.datos.message[e].total,
                "Venta Total Neta": this.datos.message[e].total_sales,
                "Gravadas 13%":this.datos.message[e].thirteen_percent_sales,
                "Gravadas 1%":this.datos.message[e].one_percent_sales,
                "Con Derecho":this.datos.message[e].with_credit_percent_sales,
                'Sin Derecho':this.datos.message[e].without_credit_percent_sales,

                 type:this.datos.message[e].type,
                products_or_services: this.datos.message[e].products_or_services
            }
            this.element.push(value);
           
              this.verprorrata(this.datos.message[e].type_prorata);
              this.sumaIVADeducible13(this.datos.message[e].thirteen_percent_sales);
              this.sumaIVADeducible1(this.datos.message[e].one_percent_sales);
              this.sumaTotal(this.datos.message[e].tax_base);
                this.sumaTotalIva(this.datos.message[e].tax);
               this.sumaTotalCompra(this.datos.message[e].total);
                  this.sumaTotalIVASoportado(this.datos.message[e].with_credit_percent_sales);
                this.sumaTotalIVADeducible(this.datos.message[e].without_credit_percent_sales);
                 this.sumaTotalGasto(this.datos.message[e].expenses);
                
          })
            this.element.sort((a, b) => {
         const isAsc = 'asc';
         return this.compare(b.Nombre, a.Nombre, isAsc);
       })
       this.dataSource = new MatTableDataSource(this.element);
       this.dataSource.paginator = this.paginator;
       return this.dataSource; 
    
      
      }else{
         this.toastr.error(this.datos.message);
       }}); }, 100);
  
  
   }

  public compare(a, b, isAsc) {
    return (a > b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public  CallLink(link) {
    window.open(link);
  }

  public openDialog(id, name) {
  
  }
  vefecha(){
    let a;
    var esp = " ";
    a = this.date.value._d;
    var b = JSON.stringify(a); 
    var c = b.split("-",2);
    var k = c[0].slice(1);
    var z = c[1]
    this.fecha = z+"/"+k;
      console.log(this.fecha);
    localStorage.setItem("fecha", JSON.stringify(this.fecha));
   // this.loadDataTable();
    }

sumaTotal(a){
   this.TotalVentaNeta =  this.TotalVentaNeta + a;
    return this.TotalVentaNeta.toFixed(2);
}
sumaTotalIva(a){
   this.TotalIVA =  this.TotalIVA + a;
    return this.TotalIVA.toFixed(2);
}
sumaTotalCompra(a){
   this.TotalVenta =  this.TotalVenta + a;
    return this.TotalVenta.toFixed(2);
}
sumaTotalIVASoportado(a){
   this.IVASoportado =  this.IVASoportado + a;
    return this.IVASoportado.toFixed(2);
}
sumaTotalIVADeducible(a){
   this.IVADeducible =  this.IVADeducible + a;
    return this.IVADeducible.toFixed(2);
    
}
sumaTotalGasto(a){
   this.Gasto =  this.Gasto + a;
    return this.Gasto.toFixed(2);    
}
sumaIVADeducible13(a){
   this.IVA13 =  this.IVA13 + a;
    return this.IVA13.toFixed(2);    
}
sumaIVADeducible1(a){
   this.IVA2 =  this.IVA2 + a;
    return this.IVA2.toFixed(2);    
}
sumaIVAprorrateado(a){
   
}
verprorrata(b){

    if(b == "special"){
     
        this.cont = this.cont+1;
       

    }else{
      this.cont = this.cont;
    }
    
}

 onlyNumberKey(event) {
  return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
reloadComponent(){
  console.log('refrescado');
  //hack para refrescar vista
  const currentUrl = this.router.url;
  const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
  this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
}
}



