import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MaterialModule } from '../../../material/material';
import { HomeTableComponent} from './home-table/home-table.component';
import { HomeFormComponent } from './home-form/home-form.component';
import { HomeDialogDeleteComponent } from './dialog/home-dialog-delete/home-dialog-delete.component';
import { HomeServices } from './home-services/home.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeEntradaComponent } from './home-entrada/home-entrada.component';
import { HomeIvaComponent } from './home-iva/home-iva.component';
import { HomeAnualComponent } from './home-anual/home-anual.component';
import { HomeRentaComponent } from './home-renta/home-renta.component';
import { HttpClientModule} from "@angular/common/http";
import { HomeIvaSevice } from './home-iva/home-iva.service.';
import { HomeManualComponent} from './home-form-manual/home-form-manual.component';
@NgModule({
  declarations: [HomeComponent, HomeTableComponent,HomeIvaComponent,
    HomeDialogDeleteComponent, HomeFormComponent,HomeEntradaComponent,
     HomeRentaComponent , HomeAnualComponent,HomeManualComponent
    ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MDBBootstrapModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  
  providers: [ HomeServices,HomeIvaSevice ],
  entryComponents: [ HomeDialogDeleteComponent, HomeComponent ],
   schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class HomeModule { }
