import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAnualComponent } from './home-anual.component';

describe('HomeAnualComponent', () => {
  let component: HomeAnualComponent;
  let fixture: ComponentFixture<HomeAnualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAnualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAnualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
