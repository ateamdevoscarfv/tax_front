import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 

import { HomeIvaSevice } from '../home-iva/home-iva.service.';
var imprime;
@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent  implements OnInit  {

  respuesta ;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
public urlconfig = environment.api+"/getConfiguration";
    public url = environment.api+"/uploadFiles";
  public imprime2; 
  public userbasic;
  verproductos = false;
  infos = {
    info : {
        type_report : "",
        number_of_files : "",
        files : {
        
            file:{
                type_file: "",
                xml: ""
            }
        }
    }

    
  };
  infos2 = {
    info : {

        confirm: {
         iva_sales_products:[]
       },
        type_report : "",
        number_of_files : "",
        files : {
        
            file:{
                type_file: "",
                xml: ""
            }
        }
    }

    
  };
  public urlvaventa = environment.api+"/getAllTypesIvaSale";
 ventporsale;
 ivas = [];config ;

constructor(public homeIva: HomeIvaSevice, public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
  this.userbasic = JSON.parse(localStorage.getItem("usuario"));
  this.http.getdevice(this.urlvaventa,this.userbasic.info).subscribe(data => {
    this.ventporsale = data;
   });
    this.http.getdevice(this.urlconfig,this.userbasic.info).subscribe(data => {
        this.config = data;  

    }) ;
 }
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
           documento: ['',Validators.required]
        
      });
       this.secondFormGroup = this._formBuilder.group({
           porcentajeV: ['']
        
      });
        this.userbasic = JSON.parse(localStorage.getItem("usuario"));
    }
    public carga(a){
        this.imprime2 = a;
        console.log(this.imprime2);
        
    }
    subir(evt){
        
         var reader = new FileReader();
         var arch = evt.target.files; // FileList object
              
                  // Loop through the FileList and render image files as thumbnails.
                  for (var i = 0, f; f = arch[i]; i++) {
              
                    // Only process image files.
                    if (!f.type.match('xml.*')) {
                      continue;
                    }
              
                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                      return function(e) {
                        // Render thumbnail.
                        
                       imprime = [ e.target.result,
                                          ].join('');
                      
                      };
                    })(f);
                         this.toastr.success("Carga de Archivo Exitosa") 
                    // Read in the image file as a data URL.
                    reader.readAsText(f);
                  } 
        
    }
    guardar(){
     
        this.infos = {
            info : {
                type_report : "iva",
                number_of_files : "1",
                files :{
                     file:{
                         type_file: this.firstFormGroup.value.documento,
                         xml: imprime
                     }
                }}
       };

     this.http.postdevice(this.url,this.infos,this.userbasic.info).subscribe(data => {
            this.respuesta = data;
            if(this.respuesta.status != "Error"){
         this.toastr.success(this.respuesta.message);
         this.reloadComponent();
       }
         else{
          this.toastr.success(this.respuesta.message);
         }
         
         
     }, err => {
         this.toastr.error("Fallo el Registro")
     })
   
    };

     confirmariv(){
      
      
        this.ivas.push(this.secondFormGroup.value.porcentajeV);
          console.log(this.ivas);
      }
       
  
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
}