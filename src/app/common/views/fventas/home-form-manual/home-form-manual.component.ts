import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 

import { HomeIvaSevice } from '../home-iva/home-iva.service.';
import {MatDatepicker} from '@angular/material/datepicker';
var imprime;
@Component({
  selector: 'app-home-manual',
  templateUrl: './home-form-manual.component.html',
  styleUrls: ['./home-form-manual.component.scss']
})
export class HomeManualComponent  implements OnInit  {
  
    date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  public url = environment.api+"/uploadFilesManually";
  public imprime2; 
  public userbasic;
  public cargaProducto = [];
  public urldocument = environment.api+"/getTypeDocumentId";
  public urltipofactura = environment.api+"/getTypeMeasureUnit";
  public urlmetpagos = environment.api+"/getTypeReport";
  public urlTipoImpuesto = environment.api+"/getTypesIvaSale";
  public documentos;
  public tipoFacturas;
  public urlconfig = environment.api+"/getConfiguration";
  public urlvaventa = environment.api+"/getAllTypesIvaSale";

  infos = {
      info : {
               activity_code: "",
              client_document_number: "",
              date: "",
              consecutive_code:"" ,
              client_name:"",
              type_document_number_id:"",
              type_document_id:"",
              change_value:"",
              products_or_services:[],
               bi:0,
               tax:0,
               total:0
                }
    
    };
  ventporsale;
  metPagos;
  ivatipos;
  config;
  ventpor;compor;

constructor(public homeIva: HomeIvaSevice, public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { 
  this.userbasic = JSON.parse(localStorage.getItem("usuario"));
  this.http.getdevice(this.urldocument,this.userbasic.info).subscribe(data => {
    this.documentos = data;
  });
  this.http.getdevice(this.urltipofactura,this.userbasic.info).subscribe(data =>{
    this.tipoFacturas = data;
  });
  this.http.getdevice(this.urlmetpagos,this.userbasic.info).subscribe(data =>{
    this.metPagos = data;
  });
  this.http.getdevice(this.urlTipoImpuesto,this.userbasic.info).subscribe(data =>{
    this.ivatipos = data;
  });
   this.http.getdevice(this.urlconfig,this.userbasic.info).subscribe(data => {
        this.config = data;  
         this.ventpor = this.config.message.iva_sale;
      this.compor = this.config.message.iva_sale;
    }) ;
   this.http.getdevice(this.urlvaventa,this.userbasic.info).subscribe(data => {
    this.ventporsale = data;
   })


}
  
    ngOnInit() {

      this.firstFormGroup = this._formBuilder.group({
        actividad: ['', Validators.required],
        proveedor: ['', Validators.required],
         cedula: ['', Validators.required],
          nfactura: ['', Validators.required],
          fecha1: ['', Validators.required],
          fecha2: ['', Validators.required],
          cambio: ['', Validators.required],
          tdoc: ['', Validators.required],
          tfac : ['', Validators.required]
      });
      this.secondFormGroup = this._formBuilder.group({
        
        nombre: ['', Validators.required],
        porcentajeC: ['', Validators.required],
        porcentajeV: ['', Validators.required],
        excento: [0, Validators.required],
        cantidad: [0, Validators.required],
        bi: [0, Validators.required],
        total: [0, Validators.required],
        pvp: [0, Validators.required],

      });
        
    }
    agregarProducto(){
       let credir = 0;
       
      if(this.secondFormGroup.valid){
        const cargatemp = {
           
            name : this.secondFormGroup.value.nombre,
            iva_purchase : this.secondFormGroup.value.porcentajeC,
            iva_sale: this.secondFormGroup.value.porcentajeV,
            percent_of_exoneration: this.secondFormGroup.value.excento,
            quantity: this.secondFormGroup.value.cantidad,
            amount_tax: this.secondFormGroup.value.bi,
            amount_bt: this.secondFormGroup.value.pvp,
            total: this.secondFormGroup.value.total,
            credit_fiscal:  this.fiscal(this.secondFormGroup.value.bi)
        };
        this.cargaProducto.push(cargatemp);
        console.log(this.cargaProducto);
      }else{
        this.toastr.error("Debe rellenar todos los campos")
      }
    }
    fiscal(a){
      if(a > 0 ){
                   return 1;
               }  else {
                  return 0;
               }
    }
    eliminarProducto(a){
   
      for(let i = 0; i< Object.keys(this.cargaProducto).length; i++){
        if( this.cargaProducto[i].id == a){
         this.cargaProducto.splice(i,1);
         console.log(this.cargaProducto);
        }
      }
      
      }
    
    public carga(a){
        this.imprime2 = a;
        console.log(this.imprime2);
        
    }
  
    subir(evt){
        
         var reader = new FileReader();
         var arch = evt.target.files; // FileList object
              
                  // Loop through the FileList and render image files as thumbnails.
                  for (var i = 0, f; f = arch[i]; i++) {
              
                    // Only process image files.
                    if (!f.type.match('xml.*')) {
                      continue;
                    }
              
                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                      return function(e) {
                        // Render thumbnail.
                        
                       imprime = [ e.target.result,
                                          ].join('');
                      
                      };
                    })(f);
                        
                    // Read in the image file as a data URL.
                    reader.readAsText(f);
                  } 
        
    }
    guardar(){
     
    

          this.infos = {
                  info : {
               activity_code: this.firstFormGroup.value.actividad,
              client_document_number: this.firstFormGroup.value.cedula,
              date: this.firstFormGroup.value.fecha1,
              consecutive_code: this.firstFormGroup.value.nfactura,
              client_name: this.firstFormGroup.value.proveedor,
              type_document_number_id:this.firstFormGroup.value.tdoc,
              type_document_id: this.firstFormGroup.value.tfac,
              change_value:this.firstFormGroup.value.cambio,
              products_or_services:this.cargaProducto,
               bi:0,
               tax:0,
               total:0
             
        }
    };
        this.http.postdevice(this.url,JSON.stringify(this.infos),this.userbasic.info).subscribe(date =>{
          this.toastr.success("Registro Exitoso");
          this.reloadComponent();
        });
    }
  
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }

    onlyDecimalNumberKey(event) { 
      let charCode = (event.which) ? event.which : event.keyCode; 
      if (charCode != 46 && charCode > 31 
       && (charCode < 48 || charCode > 57)) 
       return false; 
      return true; 
  } 
}