import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeManualComponent } from './home-form-manual.component';

describe('HomeManualComponent', () => {
  let component: HomeManualComponent;
  let fixture: ComponentFixture<HomeManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
