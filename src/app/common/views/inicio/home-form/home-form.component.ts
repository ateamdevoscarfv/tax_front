import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 
import { tablereload } from '../home-table/home-table.component';
import { HomeIvaSevice } from '../home-iva/home-iva.service.';
var imprime;
@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.scss']
})
export class HomeFormComponent  implements OnInit  {


  firstFormGroup: FormGroup;
    public url = environment.api+"/uploadFiles";
  public imprime2; 
  public userbasic;
  infos = {
    info : {
        type_report : "",
        number_of_files : "",
        files : {
        
            file:{
                type_file: "",
                xml: ""
            }
        }
    }

    
  };

constructor(public homeIva: HomeIvaSevice, private table: tablereload, public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) { }
  
    ngOnInit() {
      this.firstFormGroup = this._formBuilder.group({
        archivo: ['', Validators.required],
        documento: ['',Validators.required]
        
      });
        this.userbasic = JSON.parse(localStorage.getItem("usuario"));
    }
    public carga(a){
        this.imprime2 = a;
        console.log(this.imprime2);
        
    }
    subir(evt){
        
         var reader = new FileReader();
         var arch = evt.target.files; // FileList object
              
                  // Loop through the FileList and render image files as thumbnails.
                  for (var i = 0, f; f = arch[i]; i++) {
              
                    // Only process image files.
                    if (!f.type.match('xml.*')) {
                      continue;
                    }
              
                    // Closure to capture the file information.
                    reader.onload = (function(theFile) {
                      return function(e) {
                        // Render thumbnail.
                        
                       imprime = [ e.target.result,
                                          ].join('');
                      
                      };
                    })(f);
                        
                    // Read in the image file as a data URL.
                    reader.readAsText(f);
                  } 
        
    }
    guardar(){
        this.infos = {
            info : {
                type_report : "iva",
                number_of_files : "1",
                files :{
                     file:{
                         type_file: this.firstFormGroup.value.documento,
                         xml: imprime
                     }
                }}
    };
        
     this.http.postdevice(this.url,this.infos,this.userbasic.info).subscribe(data => {
            console.log(data);
            this.homeIva.opciontabla();
         this.toastr.success("Registro Exitoso");
         
         
     }, err => {
         this.toastr.error("Fallo el Registro")
     })
    }
  
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
}