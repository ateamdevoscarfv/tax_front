import { Component, OnInit, ViewChild, Input,Output } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { HomeServices } from '../home-services/home.service';
import { HomeDialogDeleteComponent } from '../dialog/home-dialog-delete/home-dialog-delete.component';
import { HttpServices } from '../../../http/httpServices/httpServices';
import { environment } from '../../../../../environments/environment';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-home-table',
  templateUrl: './home-table.component.html',
  styleUrls: ['./home-table.component.scss']
})
export class HomeTableComponent implements OnInit {
     tabladatos;
    element;
    public envio;
      dataSource;
      usuario;
      public urldelete = environment.api+"/deleteInvoice/"
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(HomeDialogDeleteComponent) dynamic: HomeDialogDeleteComponent;
 

  contacto: FormGroup;

  nombre;
  fecha;
  expandedElement: null;

  columnsToDisplay = [];

public url ;

 // expandedElement: ClientElement | null;
  ClientElement = [];
  constructor(
    private HomeServices: HomeServices,
    public dialog: MatDialog, public router: Router,
    public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder) {
     this.envio = JSON.parse(localStorage.getItem("envio"));
     this.usuario = JSON.parse(localStorage.getItem("usuario"));
    }
    

  ngOnInit() {
   this.columnsToDisplay = this.envio.columnas;
   this.fecha = this.envio.fecha;
   this.nombre = this.envio.nombre;  
   this.tabladatos = this.envio.datos;
   setTimeout(() => {
     this.loadDataTable();
   }, 200);
  }
  delete(id){
    this.HomeServices.deletedevice(this.urldelete+id,this.usuario.info).subscribe(data =>{
      this.toastr.success("Documento Eliminado")
      this.loadDataTable();
    },err => {
      this.toastr.error("Error al eliminar")
    }
    )
  }
  public loadDataTable() {
              this.element = this.tabladatos;
    
 
       this.element.sort((a, b) => {
         const isAsc = 'asc';
         return this.compare(b.Nombre, a.Nombre, isAsc);
       });
 
       this.dataSource = new MatTableDataSource(this.element);
       this.dataSource.paginator = this.paginator;
       return this.dataSource;
       
 
   }

  public compare(a, b, isAsc) {
    return (a > b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public  CallLink(link) {
    window.open(link);
  }

  public openDialog(id, name) {
      /**
    this.HomeServices.getdevice(this.urlantena).subscribe(data =>{
      var datos = data;
       if (Object.keys(datos).length == 0){
        this.dialog.open(HomeDialogDeleteComponent, {
          width: '250px',
          data: {name: name, id: id}
        });
       }else{
         this.toastr.error("Este dispositivo esta activo en un acceso, no puede ser eliminado");
       }
    })
    console.log(id);*/
  
  }


 onlyNumberKey(event) {
  return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}
  
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class tablereload {
  envio;
  element;
  tabladatos;
  dataSource;
  columnsToDisplay ; fecha;nombre;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public router: Router,public http: HttpClient){
    this.envio = JSON.parse(localStorage.getItem("envio"));
  }
  public loadDataTable() {
    this.element = [];
    this.columnsToDisplay = this.envio.columnas;
   this.fecha = this.envio.fecha;
   this.nombre = this.envio.nombre;  
   this.tabladatos = this.envio.datos;
    this.element = this.tabladatos;


this.element.sort((a, b) => {
const isAsc = 'asc';
return this.compare(b.Nombre, a.Nombre, isAsc);
});

this.dataSource = new MatTableDataSource(this.element);
this.dataSource.paginator = this.paginator;
return this.dataSource;

}

public compare(a, b, isAsc) {
  return (a > b ? -1 : 1) * (isAsc ? 1 : -1);
}
  reloadComponent(){
    console.log('refrescado');
    //hack para refrescar vista
    const currentUrl = this.router.url;
    const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
    this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
  }
}


