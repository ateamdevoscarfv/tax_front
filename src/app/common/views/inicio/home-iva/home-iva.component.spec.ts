import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeIvaComponent } from './home-iva.component';

describe('HomeIvaComponent', () => {
  let component: HomeIvaComponent;
  let fixture: ComponentFixture<HomeIvaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeIvaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeIvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
