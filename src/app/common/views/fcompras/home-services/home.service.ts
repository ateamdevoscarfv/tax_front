import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { UtilsService } from '../../../utils/services/utils.service';
import { ToastrService } from 'ngx-toastr';
import { HomeModel } from '../home-models/home.interface';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeServices {
 constructor(
   public http: HttpClient
 ) { }  

  getdevice(url, herders) {
    return this.http.get(url,{headers: herders});
}
postdevice(url, env, herders) {
    return this.http.post(url, env, {headers: herders});
}
deletedevice(url, herders) {
   return this.http.delete(url,{headers: herders});
}
updatedevice(url, env, herders) {
   return this.http.put(url, env, {headers: herders});
}
}