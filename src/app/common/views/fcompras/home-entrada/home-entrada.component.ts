import { Component, OnInit,Input } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { HomeServices } from '../home-services/home.service';
import { Router,ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { devicesMsg  } from '../../../utils/const/message';
import { UtilsService } from '../../../utils/services/utils.service';
import {ErrorStateMatcher} from '@angular/material/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import { HomeIvaComponent } from '../home-iva/home-iva.component';
import { HomeRentaComponent } from '../home-renta/home-renta.component';
import { HomeAnualComponent } from '../home-anual/home-anual.component';
// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';

// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment, Moment} from 'moment';

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
export interface Food {
  value: string;
  viewValue: string;
};


@Component({
  selector: 'app-entrada',
  templateUrl: './home-entrada.component.html',
  styleUrls: ['./home-entrada.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ]
})
export class HomeEntradaComponent implements OnInit {
public fecha = "" ;   
tramite = "iva";
  public contenido = true;   
public iva = false;
public renta = false;
public anual = false;
  fechaGroup: FormGroup;
  foods: Food[] = [
    {value: 'iva', viewValue: 'Declaración del Iva'},
    {value: 'anual', viewValue: 'Declaración Anual de Clientes y Proveedores'},
    {value: 'renta', viewValue: 'Declaración de Renta'}
  ];  
date = new FormControl(moment());

@Input() tabladatos;
@Input() userbasic;
@Input() urlgetcompra = environment.api+"/getPurchases/";
@Input() urlgetventa = environment.api+"/getSales/";
public ventas = false;
envio = {
    datos: '',
    fecha:'',
    usuario: '',
    columnas: [],
    nombre:''
};
datos: any;

  chosenYearHandler(normalizedYear: Moment) {
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }
constructor( public http: HomeServices, public router: Router, public toastr: ToastrService,private utils: UtilsService,public _formBuilder: FormBuilder, private route: ActivatedRoute) { }
  
    ngOnInit() {
      this.fechaGroup = this._formBuilder.group({
        documento: ['', Validators.required],
        tipo :['',Validators.required]
      });
    
    }
    vefecha(){
        let a;
        var esp = " ";
        a = this.date.value._d;
        var b = JSON.stringify(a); 
        var c = b.split("-",2);
        var k = c[0].slice(1);
        var z = c[1]
        this.fecha = z+"/"+k;
        localStorage.setItem("fecha", JSON.stringify(this.fecha));
        
        }

    comenzarborradorD1(){
        if(this.fechaGroup.value.documento == 'iva' ){
            
             this.contenido = false;
            this.iva = true;
            this.anual = false;
            this.renta = false;
        } 
         if(this.fechaGroup.value.documento == 'anual' ){
             this.contenido = false;
            this.iva = false;
            this.anual = true;
            this.renta = false;
            
        } 
         if(this.fechaGroup.value.documento == 'renta' ){
            
            this.contenido = false;
            this.iva = false;
            this.anual = false;
            this.renta = true; 
        } 
       
    }
    volver(){
        this.contenido = true;
            this.iva = false;
            this.anual = false;
            this.renta = false; 
       
    }
    opciontabla() {
          this.http.getdevice(this.urlgetcompra+this.fecha,this.userbasic.info).subscribe(data =>{
           this.datos = data;  
           if(this.datos.status != "Error" ){

           console.log(data);
               Object.keys(this.datos.message).forEach((e) => {
                 this.tabladatos =  [];
                  const value = {
                     id : this.datos.message[e].id,
                     "Condición IVA" : this.datos.message[e].receiver,
                     Incluir: this.datos.message[e].receiver,
                     Tipo: this.datos.message[e].receiver,
                     "#Comprobante": this.datos.message[e].code,
                     "Cédula":this.datos.message[e].receiver,
                     Nombre: this.datos.message[e].sender,
                     Fecha: this.datos.message[e].date,
                     "Monto neto": this.datos.message[e].tax_base,
                     "Monto IVA": this.datos.message[e].taxt,
                     "Total Crédito":this.datos.message[e].total ,
                     "Total Gasto":this.datos.message[e].receiver,
                      "Acción Realizada":this.datos.message[e].receiver,
                      
                 };
      
      this.tabladatos.push(value);
                   console.log(this.tabladatos);
    });}
    this.envio = {
     datos: this.tabladatos,
     fecha: this.fecha,
     usuario: this.userbasic.info,
     columnas:['Condición IVA','Incluir','Tipo',"#Comprobante",'Cédula','Nombre','Fecha','Monto Neto','Monto IVA','Total Crédito','Total Gasto','Acción Realizada','Acciones'],
     nombre: "Compras"
 };
 this.ventas = false;
  localStorage.setItem("envio", JSON.stringify(this.envio)); 
          });
      

  }
   
    onlyNumberKey(event) {
      return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    }
    reloadComponent() {
      console.log('refrescado');
      const currentUrl = this.router.url;
      const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
      this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
  }
