import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    constructor(private router: Router) { }
    /**
     * reload page method
     */
    reloadComponent() {
        const currentUrl = this.router.url;
        const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
        this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
    }
    public compare(a, b, isAsc) {
        return (a > b ? -1 : 1) * (isAsc ? 1 : -1);
    }
    /**
     * filter method
     * @param value to find
     * @param array array of values
     * @param field field of array
     * returns a new array with coincidences
     */
    public _filter(value: string, array: any, field: string): any[] {
        const filterValue = value.toLowerCase();
        return array.filter(rol => rol[field].toLowerCase().indexOf(filterValue) === 0);
    }
    /**
     * set localstorage method
     * @param varname name of variable to store in localstorage
     * @param values value or values to store in localstorage
     */
    public SetLocalStorage(varname: string, values: any) {
        localStorage.setItem(varname, JSON.stringify(values));
    }
    /**
     * get localstorage method
     * @param varname name of variable in localstorage
     * returns values of variable in localstorage
     */
    public GetLocalStorage(varname: string) {
        return JSON.parse(localStorage.getItem(varname));
    }
    /**
     * get item of Array / Object method
     * @param json Array / Object
     * @param parameter name of field of Array or Object property
     */
    getlist(json: any, parameter: string) {
        const value = json[parameter];
        return value;
    }
    /**
     *  convert image URL to Base 64 method
     * @param imageUrl URL
     */
    async getBase64ImageFromUrl(imageUrl) {
        const res = await fetch(imageUrl);
        const blob = await res.blob();

        return new Promise((resolve, reject) => {
            const reader  = new FileReader();
            reader.addEventListener('load', function () {
                resolve(reader.result);
            }, false);

            reader.onerror = () => {
                return reject(this);
            };
            reader.readAsDataURL(blob);
        });
    }
    /**
     * simple filter array method
     * @param value variable to search
     * @param array array target
     */
    _filtro(value: string, array: any): any[] {
        const filterValue = value.toLowerCase();
        return array.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
    }
    /**
     * open link in a new window method
     * @param link url to open
     */
    public  CallLink(link) {
        window.open(link);
    }
    /**
     * Funcion para evitar que el usuario introduzca números
     * @param value valor que viene del input por lo general $event en el (keypress)="onlyNumbers($event)"
     */
    onlyNumbers(value) {
        const tecla = (document.all) ? value.keyCode : value.which;
        if (tecla === 8) {return true; } // para la tecla de retroseso
        else if (tecla === 0 || tecla === 9) { return true; }
        // <-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla se esta transformando a 0 asi que porsiacaso los dos
        // const patron = '/[0-9\s]/'; // -> solo letras
        const patron = /[0-9,]/; // /[0-9\s]/; -> solo numeros // -> solo numeros y coma
        const te = String.fromCharCode(tecla);
        return patron.test(te);
    }
}
