export const clientsMsg = {
    save:          'Registro creado de forma exitosa',
    update:        'Registro modificado de forma exitosa',
    delete:        'Registro eliminado de forma exitosa',
    get:           'Cliente encontrado',
    errorProcess:  'Proceso rechazado'
};

export const driversMsg = {
    save:          'Registro creado de forma exitosa',
    update:        'Registro modificado de forma exitosa',
    delete:        'Registro eliminado de forma exitosa',
    get:           'Conductor encontrado',
    errorProcess:  'Proceso rechazado'
};

export const devicesMsg = {
    save:       'Dispositivo guardado con éxito',
    update:     'Dispositivo actualizado con éxito',
    delete:     'Dispositivo borrado con éxito',
    deleteFail: 'Error de servidor',
    get:        'Dispositivo encontrado'

};

export const accessMsg = {
    save:   'Acceso guardado con éxito',
    update: 'Acceso actualizado con éxito',
    delete: 'Acceso borrado con éxito',
    get:    'Acceso encontrado',
    updateFail: 'Error Acceso No Modificado ',
    saveFail: 'Error Acceso No Creado',
    valido: 'El Acceso debe tener Activador'
};

export const loginMsg = {
    login:   'Inicio de sesión exitoso',
    loginError: 'Credenciales Inválidas',
    conexionError: 'Error de conexíon',
    sessionError: 'No se pudo iniciar sesión',
    loginAccesoError: 'Usuario bloqueado, consulte al administrador'
};

export const usersMsg = {
    save:   'Usuario guardado con éxito',
    update: 'Usuario actualizado con éxito',
    delete: 'Usuario borrado con éxito',
    get:    'Usuario encontrado',
    errorProcess:  'Proceso rechazado',
    emailError: 'Email ya esta registrado',
    changePass: 'Contraseña a cambiado de forma exitosa'
};

export const recoveryMsg = {
    success: 'Su solicitud ha sido procesada. Por favor verifique su correo.'
};

export const loadsMsg = {
    save:         'Operación de carga guardada con éxito',
    saveFail:     'Ha ocurrido un error al guardar la operación',
    update:       'Operación de carga actualizada con éxito',
    updateFail:   'Ha ocurrido un error al actualizar la operación',
    delete:       'Operación de carga borrada con éxito',
    deleteFail:   'Ha ocurrido un error al borrar la operación',
    get:          'Operación de carga encontrada',
    getFail:      'Ha ocurrido un error al obtener la operación',
    getAll:       'Operacines cargadas con éxito',
    getAllFail:   'Ha ocurrido un error al obtener las operaciones',
    getFailVehicleEndOpEmpty:     'No se encontraron registros de ultimas operaciones de este vehículo'
};

export const ContainerMsg = {
    save:         'Registro creado de forma exitosa',
    saveFail:     'Ha ocurrido un error al crear el registro',
    update:       'Registro actualizado de forma exitosa',
    updateFail:   'Ha ocurrido un error al actualizar el registro',
    delete:       'Registro borrado de forma exitosa',
    deleteFail:   'Ha ocurrido un error al borrar el registro',
    get:          'Registro encontrado de forma exitosa',
    getFail:      'Ha ocurrido un error al obtener el registro',
    getAll:       'Registro cargados de forma exitosa',
    getAllFail:   'Ha ocurrido un error al obtener los registros',
    getFailEndOperation: 'No se encontraron registros de ultimas operaciones con este contenedor'
};

export const CamsMsg = {
    save:          'Camara creada de forma exitosa',
    getAllFail:    'Ha ocurrido un error en el servidor',
    NoImage:       'El dispositivo no envió las imagenes'
};

export const GralMsg = {
    status0:         'No se puede conectar con el servidor'
};

export const TypeOfChargeMsg = {
    getError:  'Ha ocurrido un error al obtener los tipos de carga'
};

export const PlaceMsg = {
    getError: 'Ha ocurrido un error al obtener los destinos'
};

export const vehiclesMsg = {
    save:       'Vehículo guardado con éxito',
    update:     'Vehículo actualizado con éxito',
    delete:     'Vehículo borrado con éxito',
    deleteFail: 'Ha ocurrido un error al borrar los registros',
    get:        'Vehículo encontrado',
    getAllFail: 'Ha ocurrido un error al obtener los registros'
};