
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';


@Injectable()
export class HttpServicesUser {
    private autorization:any;
    constructor(
        private http: HttpClient,
        private router: Router,
    ) {
        var info = JSON.parse(localStorage.getItem('info'));
        if(!info || info.token == '') {
            this.router.navigateByUrl('login');
        }

        this.autorization =  new HttpHeaders({'Authorization': 'Bearer ' + info.token});
    }

    doGet(api: string, endpoint: string) {
        const url = api + endpoint;
        return this.http.get(url, { headers: this.autorization});
    }

    doPost(api: string, endpoint: string, body) {
        const url = api + endpoint;
        return this.http.post(url, body, {headers: this.autorization});
    }

    doPut(api: string, endpoint: string, body) {
        const url = api + endpoint;
        return this.http.put(url, body, {headers: this.autorization});
    }

    doDelete(api: string, endpoint: string) {
        const url = api + endpoint;
        return this.http.delete(url, {headers: this.autorization});
    }

    setCookie(cookie?) {
        if (cookie && cookie != 'null') {
            localStorage.setItem('VERSION_CACHE_HEADER', cookie);
        }
    }

}
