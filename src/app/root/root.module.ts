import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RootRoutingModule } from './root-routing.module';
import { RootComponent } from './root.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainContentComponent } from './main-content/main-content.component';
import { MaterialModule } from '../material/material';
import { ButtonsModule, WavesModule, DropdownModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
      RootComponent,
      MainHeaderComponent,
      MainContentComponent,
  ],
  imports: [
      CommonModule,
      RootRoutingModule,
      MaterialModule,
      ButtonsModule,
      WavesModule,
      DropdownModule
  ]
})
export class RootModule { }
