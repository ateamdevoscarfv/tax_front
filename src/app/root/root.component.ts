import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    
    <app-main-content></app-main-content>`,
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
