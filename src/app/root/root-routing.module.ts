import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootComponent } from './root.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: '../common/views/login/login.module#LoginModule'
  },       
  {
    path: 'registro',
    loadChildren: '../common/views/registro/registro.module#RegistroModule'
  },
      {path: '', component: RootComponent, children: [             
           
                {
                    path: 'perfil',
                    loadChildren: '../common/views/perfil/perfil.module#PerfilModule'
                  },
                  { 
                    path: 'home',
                   loadChildren: '../common/views/home/home.module#HomeModule'
              
                  },
                  { 
                    path: 'propor',
                   loadChildren: '../common/views/proporcionalidad/home.module#HomeModule'
              
                  },
                  { 
                    path: 'fventas',
                   loadChildren: '../common/views/fventas/home.module#HomeModule'
              
                  },
                  { 
                    path: 'fcompras',
                   loadChildren: '../common/views/fcompras/home.module#HomeModule'
              
                  },
                  { 
                    path: 'resumen',
                   loadChildren: '../common/views/resumen/perfil.module#PerfilModule'
              
                  },
                  { 
                    path: 'cierres',
                   loadChildren: '../common/views/cierres/home.module#HomeModule'
              
                  },
                  { 
                    path: 'borradores',
                   loadChildren: '../common/views/borradores/home.module#HomeModule'
              
                  },
                  { 
                    path: 'gborradores',
                   loadChildren: '../common/views/gborradores/home.module#HomeModule'
              
                  },
                  { 
                    path: 'config',
                   loadChildren: '../common/views/config/perfil.module#PerfilModule'
              
                  }
        ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RootRoutingModule { }
