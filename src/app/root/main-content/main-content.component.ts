import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-main-content',
    templateUrl: './main-content.component.html' ,
    styleUrls: ['./main-content.component.scss']
  })
export class MainContentComponent implements OnDestroy {
  mobileQuery: MediaQueryList;

  // fillerNav = Array.from({length: 50}, (_, i) => `Nav Item ${i + 1}`);

  // fillerContent = Array.from({length: 50}, () =>
  //     `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
  //      labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
  //      laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
  //      voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
  //      cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`);

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private breakpointObserver: BreakpointObserver, private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  shouldRun = true;

  salir(){ 
        localStorage.removeItem('usuario');
        localStorage.clear();
        this.router.navigateByUrl('login');
        }
}











// import { Component, OnInit, Input } from '@angular/core';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
// import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
// import { Router, ActivatedRoute } from '@angular/router';


// @Component({
//   selector: 'app-main-content',
//   templateUrl: './main-content.component.html' ,
//   styleUrls: ['./main-content.component.scss']
// })
// export class MainContentComponent implements OnInit {
  
//   isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
//     .pipe(
//       map(result => result.matches)
//     );
//     usuario ;
//     @Input() valido = false;
//     @Input() name = 'Anonimo';
//   constructor(private breakpointObserver: BreakpointObserver, private router: Router) {}
//   ngOnInit() {
//     this.validar();
   
//   }
//   validar(){
//     this.usuario = JSON.parse(localStorage.getItem("usuario"));
//     console.log(this.usuario);
//     if(this.usuario != null ){
//       this.valido = true;
//       this.name = this.usuario.info.user
   
//     }else{
//       this.valido = false;
//       this.name = "Anonimo";
//     }
//   }
//   salir(){ 
//     localStorage.removeItem('usuario');
//     localStorage.clear();
//     this.router.navigateByUrl('login');
//     }
//     reloadComponent() {
//       console.log('refrescado');
//       const currentUrl = this.router.url;
//       const refreshUrl = currentUrl.indexOf('dashboard') > -1 ? '/' : '/';
//       this.router.navigateByUrl(refreshUrl).then(() => this.router.navigateByUrl(currentUrl));
//     }
// }
