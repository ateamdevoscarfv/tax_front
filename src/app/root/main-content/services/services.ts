import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataServices {
 constructor(
   public http: HttpClient
 ) { }  

  get(url) {
    return this.http.get(url);
}
post(url, env) {
    return this.http.post(url, env);
}
delete(url) {
   return this.http.delete(url);
}
update(url, env) {
   return this.http.put(url, env);
}
}