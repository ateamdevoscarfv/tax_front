import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Admin';
    constructor( private router: Router){
        var info = JSON.parse(localStorage.getItem('usuario'));
        if(!info || info.user == '') {
            this.router.navigateByUrl('login');
        }
    }
}
