import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { HttpClientModule } from '@angular/common/http';
import { HttpServices } from './common/http/httpServices/httpServices';
import { ToastrModule } from 'ngx-toastr';
import { HttpServicesUser } from './common/http/httpServices/httpServicesUsers';
import { HttpServicesLogin } from './common/http/httpServices/httpServicesLogin';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';


@NgModule({
  declarations: [
      AppComponent
  ],
  imports: [
 
    BrowserModule,
    AppRoutingModule,
    LoadingBarHttpClientModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    ToastrModule.forRoot()
  ],
  providers: [HttpServices, HttpServicesUser, HttpServicesLogin],
  bootstrap: [AppComponent]
})
export class AppModule { }
